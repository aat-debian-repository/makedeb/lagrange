# Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>

# From Arch Linux AUR: 
# Co-Maintainer: Roshless <pkg@roshless.com>
pkgname=lagrange
pkgver=1.14.1
pkgrel=0
pkgdesc="Beautiful Gemini Client"
url="https://git.skyjake.fi/skyjake/lagrange"
arch=('amd64')
license=("BSD")
source=(
    "https://git.skyjake.fi/skyjake/$pkgname/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
)

depends=(
    "libfribidi0"
    "libharfbuzz0b"
    "hicolor-icon-theme"
    "libunistring2"
    "openssl"
    "libpcre2-8-0"
    "libpcre2-16-0"
    "libpcre2-32-0"
    "libsdl2-2.0-0"
    "libwebp6"
    "zlib1g"
    "mpg123"
)

makedepends=(
    "cmake"
    "zip"
    "libunistring-dev"
    "libfribidi-dev"
    "libharfbuzz-dev"
    "libssl-dev"
    "libpcre2-dev"
    "libsdl2-dev"
    "libwebp-dev"
    "zlib1g-dev"
    "libmpg123-dev"
)

build() {
    cmake -B build -S "$pkgname-${pkgver}" \
        -DCMAKE_INSTALL_PREFIX='/usr' \
        -DENABLE_KERNING=OFF \
        -DTFDN_ENABLE_WARN_ERROR=OFF \
        -Wno-dev
    make -C build
}

package() {
    install -Dm644 $pkgname-$pkgver/LICENSE.md "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

    cd build
    make DESTDIR="$pkgdir" install
}

sha256sums=('56781fc948aa7d69ba76d59cbd666f79e154674255d9bb808eb21b7b0bb61e36')
